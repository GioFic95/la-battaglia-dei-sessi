# La battaglia dei sessi
 
### Applicazione sviluppata per l'esame di metodologie di programmazione 2017 presso l'Università di Roma La Sapienza.

Il progetto si basa sugli studi di Dawkins (etologo, biologo, divulgatore scientifico, saggista e attivista britannico)
pubblicati nel libro [The selfish gene](https://it.wikipedia.org/wiki/Il_gene_egoista).
 
[Qui](https://preview.c9users.io/giovannificarra95/lbds/Progetto.pdf) le specifiche del progetto.

---

Il sistema simula la crescita di una popolazione composta da quattro tipologie di individui, due per ogni sesso:
1. Morigerati
2. Avventurieri
3. Prudenti
4. Spregiudicate

Si fanno accoppiare due individui di sesso differente che generano a loro volta una coppia di figli della stessa
tipologia dei genitori.

Ad ogni persona viene associato un punteggio, che può variare ad ogni accoppiamento e determina se l’individuo può
continuare a vivere nel sistema.

![Uno screenshot del simulatore](https://88y32g.dm2302.livefilestore.com/y4mLErOUNRBxSXR6qKxmO1MaRsTGNQMNuaS23O60EvyGaOtdec0e_D1_K-oSoGQvPxsWGQvjGY-JCqimIAChJ5yYZLgmBW7biTqCpRcj2SotpUQz13Br47eydYen6i6rmZqZQWSZk6YoBvlsUFAwdYLu526zpgsKG-Rap9Ux2zTt2PmfgVhs6nCpDAN4yHQ1favpNkD5Wg3UgCtOh342iCSqA/Simulazione%204.PNG?psid=1)
_Screenshot del simulatore in esecuzione._

---

[Qui](https://preview.c9users.io/giovannificarra95/lbds/doc/index.html) la documentazione del progetto.

[Qui](https://preview.c9users.io/giovannificarra95/lbds/Relazione.pdf) la relazione completa del progetto.

_Autori: Giovanni Ficarra, Simone D'Amico, Lorenzo Ceccomancini._